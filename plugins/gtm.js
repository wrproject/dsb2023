const GTM_ID = "GTM-PDWJB3F";

export default defineNuxtPlugin(() => {
  // Disable GTM in development
  if (process.env.NODE_ENV === "development") return;

  if (!process.server) {
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
      "gtm.start": new Date().getTime(),
      event: "gtm.js",
    });
  }

  useHead({
    script: [
      {
        key: "gtm-js",
        src: `https://www.googletagmanager.com/gtm.js?id=${GTM_ID}`,
        async: true,
      },
    ],
    noscript: [
      {
        key: "gtm",
        children: `<iframe src="https://www.googletagmanager.com/ns.html?id=${GTM_ID}" height="0" width="0" style="display:none;visibility:hidden"></iframe>`,
      },
    ],
  });
});
