module.exports = {
    content: ['./components/**/*.{js,vue,ts}', './layouts/**/*.vue', './pages/**/*.vue', './plugins/**/*.{js,ts}', './nuxt.config.{js,ts}'],
    theme: {
        extend: {
            screens: {
                lg: '1136px'
            },

            colors: {
                gray: {
                    50: '#F2F2F2',
                    100: '#F7F9FB',
                    150: '#E6E8E9',
                    175: '#E9ECEE',
                    200: '#C0CCD1',
                    300: '#A4B5C0',
                    350: '#ACB6BB',
                    375: '#EDEDED',
                    400: '#233353',
                    500: '#292D32',
                    550: '#1D2023',
                    600: '#171B1D',
                    700: '#171719'
                },
                blue: {
                    100: '#12AFE3',
                    200: '#06A0BB',
                    300: '#0582AC',
                    400: '#07A2B9'
                },
                red: {
                    200: '#FF5F5F',
                    300: '#FF5252'
                }
            },

            fontFamily: {
                body: ['Manrope'],
                heading: ['Fjalla One']
            }
        }
    },
    plugins: []
}
