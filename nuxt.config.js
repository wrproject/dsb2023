export default defineNuxtConfig({
    css: ['~/assets/css/tailwind.css'],

    modules: ['nuxt-icons'],

    postcss: {
        plugins: {
            'tailwindcss/nesting': {},
            tailwindcss: {},
            autoprefixer: {}
        }
    },

    app: {
        head: {
            title: 'Wadim Rosenstein',
            meta: [
                {
                    hid: 'description',
                    name: 'description',
                    content: 'Wadim Rosenstein kandidiert für die DSB-Präsidentschaft'
                },
                {
                    hid: 'og:description',
                    property: 'og:description',
                    content: 'Wadim Rosenstein kandidiert für die DSB-Präsidentschaft'
                },
                {
                    hid: 'og:title',
                    property: 'og:title',
                    content: 'Wadim Rosenstein'
                },
                {
                    name: 'robots',
                    content: 'index follow max-image-preview:large'
                },
                {
                    name: 'og:type',
                    property: 'og:type',
                    content: 'website'
                },
                {
                    name: 'geo.country',
                    content: 'DE'
                },
                {
                    name: 'msapplication-TileImage',
                    content: 'https://dsb2023.de/mstile-144x144.png'
                },
                {
                    name: 'msapplication-config',
                    content: 'https://dsb2023.de/browserconfig.xml'
                },
                {
                    hid: 'twitter:card',
                    name: 'twitter:card',
                    content: 'summary_large_image'
                },
                {
                    hid: 'og:image',
                    name: 'og:image',
                    content: 'https://dsb2023.de/img.png'
                },
                {
                    hid: 'twitter:image',
                    name: 'twitter:image',
                    content: 'https://dsb2023.de/img.png'
                },
                {
                    hid: 'og:site_name',
                    name: 'og:site_name',
                    content: 'Wadim Rosenstein'
                },
                {
                    hid: 'og:image:width',
                    name: 'og:image:width',
                    content: '523'
                },
                {
                    hid: 'og:image:height',
                    name: 'og:image:height',
                    content: '360'
                }
            ]
        }
    }
})
